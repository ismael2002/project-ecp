//
//  VideosViewController.m
//  ECP
//
//  Created by Marc Cain on 5/11/15.
//  Copyright (c) 2015 Bolder Image. All rights reserved.
// Protector

#import "VideosViewController.h"
#import "CustomCell.h"
#import "AFNetworking/AFNetworking.h"
#import "defs.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VideosViewController ()
@property UIView* statusView;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@end

@implementation VideosViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getJsonFile];
    
    self.tableview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Asphalt-1536x2048"]];
    self.appVersion.text = $version;
    self.view.backgroundColor = [UIColor blackColor];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(deviceOrientationDidChangeNotification:)
     name:UIDeviceOrientationDidChangeNotification
     object:nil];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIColor *color = [UIColor whiteColor];
    
    if (!self.statusView)
    {
        self.statusView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 22.0)];
        [self.statusView setBackgroundColor:color];
        [self.view addSubview:self.statusView];
        [self.view bringSubviewToFront:self.statusView];
    }
    
    self.tableview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Asphalt-1536x2048"]];
}

- (void)deviceOrientationDidChangeNotification:(NSNotification*)note
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    [self checkOrientation];
}

- (void) checkOrientation
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    NSLog(@"Switched orientation: orientation is: %ld", (long)orientation);
    
    if (orientation == 1 )
    {
        self.statusView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, 22.0);
        [_tableview reloadData]; // reloading data because the status bar is shifting the image down when on landscape
    }
    
    if (orientation == 4 || orientation == 3)
    {
        self.statusView.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, 22.0);
        self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, 22, self.imageView.frame.size.width, self.imageView.frame.size.height);
        [_tableview reloadData];
    }
    
    if ($is_iPad ) {
        self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, 22, self.imageView.frame.size.width, self.imageView.frame.size.height);
        [_tableview reloadData];
    }
    
    self.moviePlayer.view.frame =  CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

#pragma mark - Scroll View Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGRect frame = self.statusView.frame;
    frame.origin.y = scrollView.contentOffset.y;
    self.statusView.frame = frame;
    
    [self.view bringSubviewToFront:self.statusView];
}

- (void) getJsonFile
{
    //real http://mobile.ecpinc.net/jsonProtector.json
    //test https://bolderapps.net/ecp/jsonAutoArmor.json
    
    
    NSString *string = [NSString stringWithFormat:$jsonURL];
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         self.webJsonDictionary = (NSDictionary *)responseObject;
         
         NSDictionary* ECP = [self.webJsonDictionary valueForKey:@"ECP"];
         NSDictionary* Websites = [ECP valueForKey:@"Videos"];
         NSArray* cellBackgroundColor = [ECP valueForKey:@"CellBackgroundColor"];
         NSArray* textBackgroundColor = [ECP valueForKey:@"CellTextBackgroundColor"];
         NSArray* textFontColorArray = [ECP valueForKey:@"CellFontColor"];
         NSArray* cellFontStyleArray = [ECP valueForKey:@"CellFont"];
         listOfThumbnailURLs = [ECP valueForKey:@"ScreenImage"];
         
         
         
         //gets all the results of the perspective keys, into an array of 1 value
         NSArray* arrayOfURLResults = [Websites valueForKey:@"URL"];
         NSArray* arrayOfTitleResults = [Websites valueForKey:@"Title"];
         
         if (cellFontStyleArray)
         {
             NSArray* array = cellFontStyleArray[0];
             NSDictionary* cellTextFontStyleDictionary = array[0];
             
             cellFontStyle = [cellTextFontStyleDictionary valueForKey:@"nameOfFont"];
             cellFontSize = [[cellTextFontStyleDictionary valueForKey:@"size"] integerValue];
         }
         
         //gets the background color of the cells
         if (cellBackgroundColor)
         {
             NSArray* array = cellBackgroundColor[0];
             NSDictionary* cellBackgroundColorDictionary = array[0];
             
             cellRed = [[cellBackgroundColorDictionary valueForKey:@"red"] doubleValue];
             cellBlue = [[cellBackgroundColorDictionary valueForKey:@"blue"] doubleValue];
             cellGreen = [[cellBackgroundColorDictionary valueForKey:@"green"] doubleValue];
             cellAlpha = [[cellBackgroundColorDictionary valueForKey:@"alpha"] doubleValue];
         }
         
         //gets the background color of the textview
         if (textBackgroundColor)
         {
             NSArray* array = textBackgroundColor[0];
             NSDictionary* textBackgroundColorDictionary = array[0];
             
             textBackgroundColorRed = [[textBackgroundColorDictionary valueForKey:@"red"] doubleValue];
             textBackgroundColorBlue = [[textBackgroundColorDictionary valueForKey:@"blue"] doubleValue];
             textBackgroundColorGreen = [[textBackgroundColorDictionary valueForKey:@"green"] doubleValue];
             textBackgroundColorAlpha = [[textBackgroundColorDictionary valueForKey:@"alpha"] doubleValue];
         }
         
         self.tableview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Asphalt-1536x2048"]];
         
         if (textFontColorArray)
         {
             NSArray* array = textFontColorArray[0];
             NSDictionary* textColorDictionary = array[0];
             
             textRed = [[textColorDictionary valueForKey:@"red"] doubleValue];
             textBlue = [[textColorDictionary valueForKey:@"blue"] doubleValue];
             textGreen = [[textColorDictionary valueForKey:@"green"] doubleValue];
             textAlpha = [[textColorDictionary valueForKey:@"alpha"] doubleValue];
         }
         
         if (arrayOfTitleResults[0])
         {
             listOfURLs = arrayOfURLResults[0];
             listOfTitles = arrayOfTitleResults[0];
         }
         
         //ProtectorLogo.png
         //https://bolderapps.net/ecp/ProtectorLogo.png
         
         //setting up initial image
         
         self.logoImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:listOfThumbnailURLs[0]]]];
         self.imageView.image = self.logoImage;
         
         
         NSLog(@"the file name is: %@", listOfThumbnailURLs[0]);
         //self.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://mobile.ecpinc.net/img/logo_autoarmor.png"]]];
         
         [_tableview reloadData];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Videos"
                                                             message:[error localizedDescription]
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
         [alertView show];
     }
     ];
    [operation start];
}

#pragma mark - tableView datasource

- (NSInteger) tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return listOfURLs.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell*) tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString*  CellIdentifier =  @"CustomCell";
    CustomCell* cell = (CustomCell*)  [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString* textForCell = listOfTitles[indexPath.row];
    cell.customLabel.text = textForCell;
    
    if (cellFontStyle.length > 0)
    {
        cell.customLabel.font = [UIFont fontWithName:cellFontStyle size:cellFontSize];
    }
    
    // textview font color
    if ((textRed + textBlue + textGreen) > 0)
    {
        if (textRed + textBlue + textGreen > 100)
        {
            
            cell.customLabel.textColor = [UIColor colorWithRed:textRed/255.0f
                                                         green:textGreen/255
                                                          blue:textBlue/255
                                                         alpha:textAlpha];
        }
        else
        {
            cell.customLabel.textColor = [UIColor colorWithRed:textRed green:textGreen blue:textBlue alpha:textAlpha];
        }
    }
    
    // cell background color
    if ((cellRed + cellBlue + cellGreen) > 0)
    {
        if (cellRed + cellBlue + cellGreen > 100)
        {
            cell.backgroundColor = [UIColor colorWithRed:cellRed/255.0f
                                                   green:cellGreen/255
                                                    blue:cellBlue/255
                                                   alpha:1.0f];
        }
        else
        {
            cell.backgroundColor = [UIColor colorWithRed:cellRed green:cellGreen blue:cellBlue alpha:cellAlpha];
        }
    } else {
        cell.backgroundColor = [UIColor clearColor];
    }
    
    // textView background color
    if ((textBackgroundColorRed + textBackgroundColorBlue + textBackgroundColorGreen) > 0)
    {
        if (textBackgroundColorRed + textBackgroundColorBlue + textBackgroundColorGreen > 100)
        {
            cell.customLabel.backgroundColor = [UIColor colorWithRed:textBackgroundColorRed/255.0f
                                                               green:textBackgroundColorGreen/255
                                                                blue:textBackgroundColorBlue/255
                                                               alpha:1.0f];
        }
        else
        {
            cell.customLabel.backgroundColor = [UIColor colorWithRed:textBackgroundColorRed green:textBackgroundColorGreen blue:textBackgroundColorBlue alpha:textBackgroundColorAlpha];
        }
    } else {
        cell.customLabel.backgroundColor = [UIColor clearColor];
    }
    
    self.imageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Asphalt-1536x2048"]];
    
    return  cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableview deselectRowAtIndexPath:indexPath animated:YES];
    
    NSURL *videoUrl = [NSURL URLWithString:listOfURLs[indexPath.row]];
    NSLog(@"The url is: %@", videoUrl);
    
    MPMoviePlayerViewController *mpvc = [[MPMoviePlayerViewController alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinished:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    mpvc.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
    [mpvc.moviePlayer setContentURL:videoUrl];
    
    [self presentMoviePlayerViewControllerAnimated:mpvc];
}

- (void)movieFinished:(NSNotification *)notifcation
{
    [self dismissViewControllerAnimated:NO completion:nil];
    
    NSLog(@"video finished");
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    NSLog(@"CHANGING THE HEIGHT of tableview header because status bar is adjusting to status bar");
    
    if (orientation == 1 && !($is_iPad) )
    {
        return 0;
    }
    
    if (orientation == 4 || orientation == 3)
    {
        return 10;
    }
    
    if ($is_iPad && (orientation == 5 || orientation ==1))
    {
        return 10;
    }
    
    return 0;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    return headerView;
}

@end