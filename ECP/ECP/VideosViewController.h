//
//  VideosViewController.h
//  ECP
//
//  Created by Marc Cain on 5/11/15.
//  Copyright (c) 2015 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface VideosViewController : UITableViewController <UITableViewDelegate, UIWebViewDelegate>
{
    //NSDictionary* jsonDictionary;
    //NSDictionary* webJsonDictionary;
    
    NSArray* listOfCategories;
    NSArray* listOfURLs;
    NSArray* listOfTitles;
    NSArray* listOfThumbnailURLs;
    NSMutableArray* imagesArray;
    
    double tableRed;
    double tableBlue;
    double tableGreen;
    double tableAlpha;
    
    double cellRed;
    double cellBlue;
    double cellGreen;
    double cellAlpha;
    
    double textRed;
    double textBlue;
    double textGreen;
    double textAlpha;
    
    double textBackgroundColorRed;
    double textBackgroundColorBlue;
    double textBackgroundColorGreen;
    double textBackgroundColorAlpha;
    
    NSInteger cellFontSize;
    NSString* cellFontStyle;
}

@property (weak, nonatomic) IBOutlet UILabel *appVersion;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSDictionary* webJsonDictionary;
@property (nonatomic, strong) UIImage* logoImage;
//@property (nonatomic, weak) BOOL videoPlaying;

@end
