//
//  CustomCell.h
//  ECP
//
//  Created by Marc Cain on 5/11/15.
//  Copyright (c) 2015 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

//@property (weak, nonatomic) IBOutlet UIWebView *cellWebview;

@property (weak, nonatomic) IBOutlet UITextView *cellTextView;
@property (nonatomic) IBOutlet UILabel* customLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageCell;


@end
