//
//  CustomCell.m
//  ECP
//
//  Created by Marc Cain on 5/11/15.
//  Copyright (c) 2015 Bolder Image. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
