//
//  defs.h
//  ECP
//
//  Copyright (c) 2012 BolderImage. All rights reserved.
//

#ifndef Culligan_defs_h
#define Culligan_defs_h


#define _ReleaseObj(obj)    if (obj){ [obj release]; obj = nil; }
#define KVersion                [[[UIDevice currentDevice] systemVersion] intValue]

#endif


/// iOS version identification
/// Usage: $ios_equal_to(@"5.0")
/// Returns: NSString
#define $ios_equal_to(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define $ios_greater_than(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define $ios_less_than(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define $jsonURL @"http://mobile.ecpinc.net/jsonAutoArmor.json"

/// iOS device identification
/// Usage: if ($is_iPad) { ... }
/// Returns: Boolean
#define $is_iPad            (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define $is_iPhone          (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define $is_retina          ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] >= 2)
#define $has_multitasking   ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)] && [[UIDevice currentDevice] isMultitaskingSupported])

/// iOS device versioning
/// Usage: cell.text = $version;
/// Returns: NSString
#define $build              [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]
#define $version            [NSString stringWithFormat:@"Ver %@ Build %@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];