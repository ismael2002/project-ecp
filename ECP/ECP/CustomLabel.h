//
//  CustomLabel.h
//  ECP
//
//  Created by Marc Cain on 7/30/15.
//  Copyright (c) 2015 Bolder Image. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLabel : UILabel

@end
